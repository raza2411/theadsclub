<?php
    //Template Name:Tac-Home-Page

    get_header(home);
?>

<!-- banner -->
        
        <section id="banner" class="wow fadeIn position-relative d-flex justify-content-center align-items-center" data-wow-duration="2s">
            <?php 
                $banner_logo = get_post_meta(get_the_ID(), 'ms_banner_logo', true);
                $color_section = get_post_meta(get_the_ID(), 'ms_color_section', true);
                $banner_text = get_post_meta(get_the_ID(), 'ms_banner_text', true);
                $banner_bg = get_post_meta(get_the_ID(), 'ms_banner_bg', true);
            ?>
            <div class="banner-logo position-absolute wow bounceIn" data-wow-duration="2s">
                <div class="holder p-2">
                    <div id="f1_container" class="m-3">
                        <div id="f1_card" >
                            <div class="front face d-flex align-items-center shadow">
                                <div class="holder-2">
                                    <img src="<?php echo $banner_logo; ?>" alt="#">
                                </div>
                            </div>
                            <a class="back face center shadow d-table scroll" href="#contact-us">
                                <p class=" d-table-cell align-middle">Enquire Now</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row h-100 w-100 m-0">
                <a class="col-sm-12 col-md-12 col-lg-6 d-flex align-items-center justify-content-center text-uppercase color-section" href="<?php the_permalink(); ?>/our-audience" style="background-color:<?php echo $color_section; ?>;">
                    <div class="px-5 wow fadeInUp" data-wow-duration="2s">
                        <h1><?php echo $banner_text; ?></h1>
                    </div>
                </a>
                <a class="col-sm-12 col-md-12 col-lg-6 d-flex align-items-center justify-content-center img-section" href="<?php the_permalink(); ?>/cup-advertising" style="background-image:url('<?php echo $banner_bg; ?>');">
                    <div class="image px-5 wow fadeInUp" data-wow-duration="2s">
                        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                            <div class="carousel-inner">
                            <?php 
                                $args = array(
                                'post_type' => 'home_slider',
                                'order' => 'DESC',
                                'posts_per_page' => -1
                                );
                                $count = 1;
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                            ?>
                           <?php if($count == 1) { 
                               $active = "active";
                              }else {
                                $active = "";
                              } ?>
                                <div class="carousel-item <?php echo $active; ?>" data-wow-delay="1s">
                                    <img class="d-block w-100" src="<?php the_post_thumbnail_url(); ?>" alt="First slide">
                                </div>
                                <?php 
                                $count++;
                                    endwhile;
                                    wp_reset_query();
                                ?>
                            </div>
                        </div>
                    </div>
                                </a>
            </div>
        </section>

        <!-- content -->

        <main>
            <div id="boxes">
                <?php
                    $box_image_1 = get_post_meta(get_the_ID(), 'ms_box_image_1', true);
                    $box_title_1 = get_post_meta(get_the_ID(), 'ms_box_title_1', true);
                    $box_detail_1 = get_post_meta(get_the_ID(), 'ms_box_detail_1', true);
                    $box_image_2 = get_post_meta(get_the_ID(), 'ms_box_image_2', true);
                    $box_title_2 = get_post_meta(get_the_ID(), 'ms_box_title_2', true);
                    $box_detail_2 = get_post_meta(get_the_ID(), 'ms_box_detail_2', true);
                    $box_image_3 = get_post_meta(get_the_ID(), 'ms_box_image_3', true);
                    $box_title_3 = get_post_meta(get_the_ID(), 'ms_box_title_3', true);
                    $box_detail_3 = get_post_meta(get_the_ID(), 'ms_box_detail_3', true);
                ?>
                <div class="row m-0 text-white ">
                    <div class="col-md-6 col-lg-4 grey py-5 px-5">
                    <ul class="d-flex align-items-center">
                        <li class="mr-4 w-50">
                            <img src="<?php echo $box_image_1; ?>">
                        </li>
                        <li>
                            <h3 class="text-capitalize"><?php echo $box_title_1; ?></h3>
                            <p><?php echo $box_detail_1; ?></p>
                        </li>
                    </ul>
                    </div>
                    <div class="col-md-6 col-lg-4 black py-5 px-5">
                    <ul class="d-flex align-items-center">
                        <li class="mr-4 w-50">
                            <img src="<?php echo $box_image_2; ?>">
                        </li>
                        <li>
                            <h3 class="text-capitalize"><?php echo $box_title_2; ?></h3>
                            <p><?php echo $box_detail_2; ?></p>
                        </li>
                    </ul>
                    </div>
                    <div class="col-md-12 col-lg-4 orange py-5 px-5">
                    <ul class="d-flex align-items-center">
                        <li class="mr-4 w-50">
                            <img src="<?php echo $box_image_3; ?>">
                        </li>
                        <li>
                            <h3 class="text-capitalize"><?php echo $box_title_3; ?></h3>
                            <p><?php echo $box_detail_3; ?></p>
                        </li>
                    </ul>
                    </div>
                    </div>
            </div>
            <section id="intro" class="text-center mb-5">
            <?php 
                $intro_title = get_post_meta(get_the_ID(), 'ms_intro_title', true);
                $i_long_detail = get_post_meta(get_the_ID(), 'ms_i_long_detail', true);
                $about_title = get_post_meta(get_the_ID(), 'ms_about_title', true);
                $about_heading = get_post_meta(get_the_ID(), 'ms_about_heading', true);
                $a_short_detail = get_post_meta(get_the_ID(), 'ms_a_short_detail', true);
                $a_long_detail = get_post_meta(get_the_ID(), 'ms_a_long_detail', true);
                $a_button_text = get_post_meta(get_the_ID(), 'ms_a_button_text', true);
                $a_image_1 = get_post_meta(get_the_ID(), 'ms_a_image_1', true);
                $a_image_2 = get_post_meta(get_the_ID(), 'ms_a_image_2', true);
            ?>
                <h4 class="wow fadeInUp" data-wow-duration="2s"><?php echo $intro_title; ?></h4>
                <P class="m-3 m-md-4 m-lg-5 wow fadeInUp" data-wow-duration="2s"><?php echo $i_long_detail; ?>
                </P>
                <div class="row about">
                    <div class="col-sm-12 col-md-12 col-lg-6 d-flex align-items-center wow fadeInUp" data-wow-duration="2s">
                        <div class="introduction px-3 px-md-3 mx-lg-5 text-left">
                            <span class="text-uppercase wow fadeInUp" data-wow-duration="2s"><?php echo $about_title; ?></span>.

                            <h3 class="title text-uppercase wow fadeInUp" data-wow-duration="2s"><?php echo $about_heading; ?></h3>

                            <p class="short-detail py-3 wow fadeInUp" data-wow-duration="2s"><?php echo $a_short_detail; ?></p>

                            <p class="detail wow fadeInUp" data-wow-duration="2s"><?php echo $a_long_detail; ?></p>
                            <a class="d-inline-block p-3 mt-4 view-project text-uppercase wow fadeInUp" data-wow-duration="2s" href="<?php the_permalink(); ?>/cup-advertising"><?php echo $a_button_text; ?></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInUp" data-wow-duration="2s">
                        <div class="fix-width">
                            <div class="about-image-2 position-absolute w-100 h-100 wow fadeInUp" data-wow-duration="2s" style="background-image:url('<?php echo $a_image_2; ?>');">
                            </div>
                            <div class="about-image-1 position-absolute w-100 h-100 wow fadeInUp" data-wow-duration="2s" style="background-image:url('<?php echo $a_image_1; ?>');">
                            </div>
                            <div class="position-absolute transparent-color transparent_class wow fadeInUp" data-wow-duration="2s">
                            </div>
                        </div>
                        <!-- <div style="clear: both"></div> -->
                    </div>
            </section>
            <section id="marketing">
            <?php
                $title = get_post_meta(get_the_ID(), 'ms_title', true);
                $heading = get_post_meta(get_the_ID(), 'ms_heading', true);
                $detail = get_post_meta(get_the_ID(), 'ms_detail', true);
                $logo = get_post_meta(get_the_ID(), 'ms_logo', true);
                $m_button_text = get_post_meta(get_the_ID(), 'ms_m_button_text', true);
               
            ?>
                <div class="row justify-content-end no-gutters pb-5 mb-5 heading-bubble">
                    <div class="col-md-12 col-lg-6 px-3 pl-md-3 heading">
                        <span class="text-capitalize d-block wow fadeInUp" data-wow-duration="2s"><?php echo $title; ?></span>
                        <h3 class="text-capitalize d-inline-block m-0 wow fadeInUp" data-wow-duration="2s"><?php echo $heading; ?></h3>
                        <div class="row mt-3">
                            <div class="col-md-6 wow fadeInUp" data-wow-duration="2s">
                                <p><?php echo $detail; ?></p>
                                <a class="d-inline-block p-3 mt-4 view-project text-uppercase wow fadeInUp" data-wow-duration="2s" href="<?php the_permalink(); ?>/our-audience"><?php echo $m_button_text; ?></a>
                            </div>
                            <div class="col-md-6 p-0 text-right wow fadeInUp" data-wow-duration="2s">
                                <img class="img-fluid" src="<?php echo $logo ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                 $blocks = get_post_meta(get_the_ID(), 'ms_blocks', true);
                 $count = 1;
                 foreach ( (array) $blocks as $key => $block ) {
                    if ( isset( $block['ms_block_number'] ) ) {
                        $block_number = esc_html( $block['ms_block_number'] );
                    }
                    if ( isset( $block['ms_block_title'] ) ) {
                        $block_title = esc_html( $block['ms_block_title'] );
                    }
                    if ( isset( $block['ms_block_heading'] ) ) {
                        $block_heading = esc_html( $block['ms_block_heading'] );
                    }
                    if ( isset( $block['ms_bottom_heading'] ) ) {
                        $bottom_heading = esc_html( $block['ms_bottom_heading'] );
                    }
                    if ( isset( $block['ms_b_short_detail'] ) ) {
                        $b_short_detail = esc_html( $block['ms_b_short_detail'] );
                    }
                    if ( isset( $block['ms_b_long_detail'] ) ) {
                        $b_long_detail = esc_html( $block['ms_b_long_detail'] );
                    }
                    if ( isset( $block['ms_b_bg_image'] ) ) {
                        $img = esc_html( $block['ms_b_bg_image'] );
                    }
                ?>
                <?php if($count % 2 == 0) { ?>
                    <div class="row justify-content-end no-gutters pb-5 mb-5 bubble-2 odd wow fadeInRight" data-wow-duration="2s">
                    <div class="col-md-10 col-lg-7  position-relative">
                        <div class="marketing-2">
                        <img class="img-fluid" src="<?php echo $img; ?>" alt="#">
                        <div class="over-lay position-absolute w-100 d-flex align-items-center justify-content-center">
                            <div class="detail text-uppercase">
                            <span class="block_number"><?php echo $count; ?></span>
                                <span class="d-block "><?php echo $block_title; ?></span>
                                <h3 class="d-block"><?php echo $block_heading; ?></h3>
                                <span class="d-block"><?php echo $bottom_heading; ?></span>
                                <p><?php echo $b_short_detail; ?></p>
                                <p class="long-detail"><?php echo $b_long_detail; ?></p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                    <div class="row no-gutters pb-5 mb-5 bubble-1 even wow fadeInLeft" data-wow-duration="2s">
                    <div class="col-md-10 col-lg-8  position-relative">
                        <div class="marketing-1">
                        <img class="img-fluid" src="<?php echo $img; ?>" alt="#">
                        <div class="over-lay position-absolute w-100 d-flex align-items-center justify-content-center">
                            <div class="detail text-uppercase">
                                <span class="block_number"><?php echo $count; ?></span>
                                <span class="d-block "><?php echo $block_title; ?></span>
                                <h3 class="d-block"><?php echo $block_heading; ?></h3>
                                <span class="d-block"><?php echo $bottom_heading; ?></span>
                                <p><?php echo $b_short_detail; ?></p>
                                <p class="long-detail"><?php echo $b_long_detail; ?></p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <?php }  ?>
                <?php $count++; } ?>
            </section>
            <section id="testimonial">
                <div class="px-3 px-md-3 px-lg-5 mx-lg-5">
                    <div class="heading text-uppercase wow fadeInUp" data-wow-duration="2s">
                        <span>testimonial</span>
                        <h3>happy clients</h3>
                    </div>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            $args = array(
                                'post_type' => 'testimonial',
                                'order' => 'ASC',
                                'posts_per_page' => -1
                            );
                            $loop = new WP_Query($args);
                            $counter = 0;
                            while ($loop->have_posts()) : $loop->the_post();
                                if ($counter == 0) {
                                 $act = "active";
                                } else {
                                    $act = "";
                                }
                                ?>
                                <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $counter; ?>" class="<?php echo $act; ?>"></li>
                                <?php
                                $counter++;
                            endwhile;
                            ?>   
                        </ol>
                        <div class="carousel-inner">
                        <?php 
                                $args = array(
                                'post_type' => 'testimonial',
                                'order' => 'DESC',
                                'posts_per_page' => -1
                                );
                                $count = 1;
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                            ?>
                           <?php if($count == 1) { 
                               $active = "active";
                              }else {
                                $active = "";
                              } ?>
                            <div class="carousel-item <?php echo $active; ?>">
                                <div class="row">
                                    <div class="col-md-4 d-flex align-items-center">
                                        <div class="avatar">
                                            <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="#">
                                        </div>
                                    </div>
                                    <div class="col-md-8 py-3 py-md-5 py-lg-5 px-3 px-md-5 px-lg-5">
                                       <?php the_content(); ?>
                                        <div class="designation">
                                            <h4><?php the_title(); ?></h4>
                                            <span><?php the_excerpt(); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                                $count++;
                                    endwhile;
                                    wp_reset_query();
                                ?>
                        </div>
                    </div>
                </div>
            </section>
            <section id="blog">
                <div class="px-3 px-md-5 px-lg-5 mx-3 mx-md-5 mx-lg-5">
                    <div class="row">
                        <?php
                            global $post;
                            $args = array( 'posts_per_page' => 4 );
                            $lastposts = get_posts( $args );
                            foreach ( $lastposts as $post ) :
                            setup_postdata( $post ); 
                        ?>
                        <div class="col-md-12 col-lg-6 px-lg-5 pb-5 wow fadeInLeft" data-wow-duration="2s">
                            <div class="row">
                                <div class="col-md-4 p-0">
                                    <div class="image wow fadeInUp" data-wow-duration="2s">
                                    <?php if ( has_post_thumbnail() ) {
                                                the_post_thumbnail();
                                        } else { ?>
                                        <img src="<?php bloginfo('template_directory')?>\assets\dist\images\none.png" alt="<?php the_title(); ?>" />
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                                <div class="col-md-8 p-0 blog-text  d-flex align-items-center position-relative wow fadeInUp" data-wow-duration="2s">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="arrow position-absolute">
                                   
                                        <i class="fas fa-arrow-right"></i>
                                      
                                    </div>
                                    </a>
                                    <div class="px-5 py-3 blog_post">
                                        <h6><?php echo chop_string(the_title(), 5); ?></h6>
                                        <p><?php echo chop_string(the_content(), 5); ?></p>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <?php
                            endforeach; 
                            wp_reset_postdata(); 
                        ?>
                    </div>
                </div>
            </section>
            <section id="contact-us" class="pt-5">
            <div class=" text-center title mb-5 pb-5 wow fadeInUp" data-wow-duration="2s">
                <h4>Get the</h4>
                <h1>Quick</h1>
                <span>Response regarding your needs</span>
            </div>
            <div class="form w-50 mx-auto p-5 mb-md-5 text-center position-relative">
           
                
                <?php echo do_shortcode( '[contact-form-7 id="3398" title="Contact form 1"]' ); ?>
                
            </div>
            </section>
        </main>

<?php get_footer(home);
