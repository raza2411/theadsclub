<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TAC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- stylesheet -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_directory')?>/assets/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory')?>/assets/dist/css/animate.css" />
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">
    <!-- fontawsome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
    <?php wp_head(home); 
    ?>
</head>

<body>
<?php 
global $avia_config;
?>

    <div id="wrapper">

        <!-- header -->

        <header>
            <?php 
            $output 	 = "";
                 
                 /*
                 *	display the theme logo by checking if the default logo was overwritten in the backend.
                 *   the function is located at framework/php/function-set-avia-frontend-functions.php in case you need to edit the output
                 */
                 $addition = false;
                 if( !empty($headerS['header_transparency']) && !empty($headerS['header_replacement_logo']) )
                 {
                     $addition = "<img src='".$headerS['header_replacement_logo']."' class='alternate' alt='' title='' />";
                 }
                 $instagram = get_post_meta('3206', 'ms_instagram', true);
                 $facebook = get_post_meta('3206', 'ms_facebook', true);
                 $twitter = get_post_meta('3206', 'ms_twitter', true);
                 $email = get_post_meta('3206', 'ms_email', true);
            ?>
            <div class="top-nav py-2 px-lg-5">
                <ul class="text-center text-lg-right wow fadeInUp" data-wow-duration="2s">
                    <li>
                        <a href="<?php echo $instagram; ?>" target="blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $facebook; ?>" target="blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $twitter; ?>">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $email; ?>">
                            <i class="fas fa-envelope"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <nav class="navbar navbar-expand-lg px-lg-5 wow fadeIn" data-wow-duration="2s">
                    <?php echo $output .= avia_logo(AVIA_BASE_URL.'images/layout/logo.png', $addition); ?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fab fa-microsoft"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                <?php
                        wp_nav_menu(
                                array(
                                    'menu' => 'Main Menu',
                                    'container' => '',
                                    'container_class' => '',
                                    'container_id' => '',
                                    'menu_class' => 'navbar-nav text-uppercase ml-auto text-center',
                                    'menu_id' => '',
                                    'link_before' => '<span class="nav-link hvr-underline-from-center p-0">',
                                    'link_after' => '</span'. '>'
                                )
                        );
                        ?>
                </div>
            </nav>
        </header>