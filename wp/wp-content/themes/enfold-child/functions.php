<?php

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
};
function add_theme_scripts() {
    wp_enqueue_style( 'mycss', get_template_directory_uri() . '/assets/dist/css/style.css');
    

  }
  add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

  function dolly_css() {
   ?>
    <style type='text/css'>
   .goto_google,.goto_demo,.goto_upload,.goto_update{
        display: none !important;
    }
    </style>
    <?php
}
add_action( 'admin_head', 'dolly_css' );

function testimonial_cpt() {
    $labels = array(
        'name'                  => _x('Testimonial Slider', 'post type general name'),
        'singular_name'         => _x('Testimonial Slider', 'post type singular name'),
        'add_new'               => _x('Add New', 'Testimonial Slider'),
        'add_new_item'          => __('Add New Testimonial Slider'),
        'edit_item'             => __('Edit Testimonial Slider'),
        'new_item'              => __('New Testimonial Slider'),
        'all_items'             => __('All Testimonial Sliders'),
        'view_item'             => __('View Testimonial Slider'),
        'search_items'          => __('Search Testimonial Slider'),
        'not_found'             => __('No Testimonial Slider found'),
        'not_found_in_trash'    => __('No Testimonial Slider found in the Trash'),
        'parent_item_colon'     => '',
        'menu_name'             => 'Testimonial'
    );
    $args = array(
        'labels'                => $labels,
        'description'           => 'Custom Testimonial',
        'public'                => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-format-quote',
        'supports'              => array('title', 'editor', 'thumbnail','excerpt'),
        'has_archive'           => true,
    );
    register_post_type('testimonial', $args);
}

add_action('init', 'testimonial_cpt');


function home_slider_cpt() {
    $labels = array(
        'name'                  => _x('Home_slider', 'post type general name'),
        'singular_name'         => _x('Home_slider', 'post type singular name'),
        'add_new'               => _x('Add New', 'Home_slider'),
        'add_new_item'          => __('Add New Home_slider'),
        'edit_item'             => __('Edit Home_slider'),
        'new_item'              => __('New Home_slider'),
        'all_items'             => __('All Home_sliders'),
        'view_item'             => __('View Home_slider'),
        'search_items'          => __('Search Home_slider'),
        'not_found'             => __('No Home_slider found'),
        'not_found_in_trash'    => __('No Home_slider found in the Trash'),
        'parent_item_colon'     => '',
        'menu_name'             => 'Home slider'
    );
    $args = array(
        'labels'                => $labels,
        'description'           => 'Custom home_slider portfolio',
        'public'                => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-images-alt',
        'supports'              => array('title', 'editor', 'thumbnail'),
        'has_archive'           => true,
    );
    register_post_type('home_slider', $args);
}

add_action('init', 'home_slider_cpt');


require_once 'meta_boxes/init.php';

add_filter( 'cmb2_meta_boxes', 'ms_metaboxes' );

function ms_metaboxes( array $meta_boxes ) {

    $prefix = 'ms_';
	$meta_boxes['single_metaboxes'] = array(
        'id' => 'single_metaboxes',
        'title' => __('Banner', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'position' => 1,
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Banner Logo',
                'desc' => '',
                'id' => $prefix . 'banner_logo',
                'type' => 'file'
            ),
            array(
                'name' => 'Background Colour',
                'desc' => '',
                'id' => $prefix . 'color_section',
                'type' => 'colorpicker'
            ),
            array(
                'name' => 'Banner Text',
                'desc' => '',
                'id' => $prefix . 'banner_text',
                'type' => 'textarea_small'
            ),
            array(
                'name' => 'Background Image',
                'desc' => '',
                'id' => $prefix . 'banner_bg',
                'type' => 'file'
            ),
        ),
    );
    $meta_boxes['boxes'] = array(
        'id' => 'boxes',
        'title' => __('Boxes', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Image',
                'desc' => '',
                'id' => $prefix . 'box_image_1',
                'type' => 'file'
            ),
            array(
                'name' => 'Title',
                'desc' => '',
                'id' => $prefix . 'box_title_1',
                'type' => 'text'
            ),
            array(
                'name' => 'Detail',
                'desc' => '',
                'id' => $prefix . 'box_detail_1',
                'type' => 'textarea_small'
            ),
            array(
                'name' => 'Image',
                'desc' => '',
                'id' => $prefix . 'box_image_2',
                'type' => 'file'
            ),
            array(
                'name' => 'Title',
                'desc' => '',
                'id' => $prefix . 'box_title_2',
                'type' => 'text'
            ),
            array(
                'name' => 'Detail',
                'desc' => '',
                'id' => $prefix . 'box_detail_2',
                'type' => 'textarea_small'
            ),
            array(
                'name' => 'Image',
                'desc' => '',
                'id' => $prefix . 'box_image_3',
                'type' => 'file'
            ),
            array(
                'name' => 'Title',
                'desc' => '',
                'id' => $prefix . 'box_title_3',
                'type' => 'text'
            ),
            array(
                'name' => 'Detail',
                'desc' => '',
                'id' => $prefix . 'box_detail_3',
                'type' => 'textarea_small'
            ),
        ),
    );
    $meta_boxes['introduction'] = array(
        'id' => 'introduction',
        'title' => __('Introduction', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Title',
                'desc' => '',
                'id' => $prefix . 'intro_title',
                'type' => 'text'
            ),
            array(
                'name' => 'Detail',
                'desc' => '',
                'id' => $prefix . 'i_long_detail',
                'type' => 'textarea_small'
            ),
            array(
                'name' => 'About Title',
                'desc' => '',
                'id' => $prefix . 'about_title',
                'type' => 'text'
            ),
            array(
                'name' => 'About Heading',
                'desc' => '',
                'id' => $prefix . 'about_heading',
                'type' => 'text'
            ),
            array(
                'name' => 'Short detail',
                'desc' => '',
                'id' => $prefix . 'a_short_detail',
                'type' => 'textarea_small'
            ),
            array(
                'name' => 'About Detail',
                'desc' => '',
                'id' => $prefix . 'a_long_detail',
                'type' => 'wysiwyg'
            ),
            array(
                'name' => 'Button Text',
                'desc' => '',
                'id' => $prefix . 'a_button_text',
                'type' => 'text'
            ),
            array(
                'name' => 'Image 1',
                'desc' => '',
                'id' => $prefix . 'a_image_1',
                'type' => 'file'
            ),
            array(
                'name' => 'Image 2',
                'desc' => '',
                'id' => $prefix . 'a_image_2',
                'type' => 'file'
			),
        ),
    );
    $meta_boxes['marketing'] = array(
        'id' => 'marketing',
        'title' => __('Marketing', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'position' => 1,
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'title',
                'desc' => '',
                'id' => $prefix . 'title',
                'type' => 'text'
            ),
            array(
                'name' => 'Heading',
                'desc' => '',
                'id' => $prefix . 'heading',
                'type' => 'text'
            ),
            array(
                'name' => 'Detail',
                'desc' => '',
                'id' => $prefix . 'detail',
                'type' => 'textarea'
            ),
            array(
                'name' => 'logo',
                'desc' => '',
                'id' => $prefix . 'logo',
                'type' => 'file'
            ),
            array(
                'name' => 'Button Text',
                'desc' => '',
                'id' => $prefix . 'm_button_text',
                'type' => 'text'
            ),
            array(
                'id'=> $prefix . 'blocks',
                'type'        => 'group',
                'description' => __( 'Blocks', 'ms' ),
                'options'     => array(
                'group_title'   => __( 'Block {#}', 'ms' ), 
                'add_button'    => __( 'Add More Block', 'ms' ),
                'remove_button' => __( 'Remove Block', 'ms' ),
                'sortable'      => true, 
                ),
                'fields'      => array(
                    array(
                        'name' => 'Block Title',
                        'desc' => '',
                        'id'   => $prefix.'block_title',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Block heading',
                        'desc' => '',
                        'id'   => $prefix.'block_heading',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Block Heading Bottom ',
                        'desc' => '',
                        'id'   => $prefix.'bottom_heading',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Block Short Detail ',
                        'desc' => '',
                        'id'   => $prefix.'b_short_detail',
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'Block Long Detail ',
                        'desc' => '',
                        'id'   => $prefix.'b_long_detail',
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => 'Background Image ',
                        'desc' => '',
                        'id'   => $prefix.'b_bg_image',
                        'type' => 'file',
                    ),
                ),
            ),
        ),
    );
    $meta_boxes['footer'] = array(
        'id' => 'footer',
        'title' => __('Footer', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'position' => 1,
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Background Image',
                'desc' => '',
                'id' => $prefix . 'f_bg_image',
                'type' => 'file'
            ),
            array(
                'name' => 'Footer Logo',
                'desc' => '',
                'id' => $prefix . 'f_logo',
                'type' => 'file'
            ),
            array(
                'name' => 'Address',
                'desc' => '',
                'id' => $prefix . 'f_address',
                'type' => 'text'
            ),
            array(
                'name' => 'Contact',
                'desc' => '',
                'id' => $prefix . 'f_contact',
                'type' => 'text'
            ),
            array(
                'name' => 'E-Mail Address',
                'desc' => '',
                'id' => $prefix . 'f_email',
                'type' => 'text'
            ),
        ),
    );
    $meta_boxes['social_links'] = array(
        'id' => 'social_links',
        'title' => __('Social Links', 'im'),
        'object_types' => array('page'), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array( 3206 ) ),
        'context' => 'normal',
        'priority' => 'default', 
        'position' => 1,
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Instagram',
                'desc' => '',
                'id' => $prefix . 'instagram',
                'type' => 'text_url'
            ),
            array(
                'name' => 'Facebook',
                'desc' => '',
                'id' => $prefix . 'facebook',
                'type' => 'text_url'
            ),
            array(
                'name' => 'Twitter',
                'desc' => '',
                'id' => $prefix . 'twitter',
                'type' => 'text_url'
            ),
            array(
                'name' => 'Email',
                'desc' => '',
                'id' => $prefix . 'email',
                'type' => 'text_url'
            ),
        ),
    );
    return $meta_boxes;
}
if (!function_exists('chop_string')) {
    function chop_string($str, $len) {
        if (strlen($str) < $len)
            return $str;
    
        $str = substr($str,0,$len);
        if ($spc_pos = strrpos($str," "))
                $str = substr($str,0,$spc_pos);
    
        return $str . "Read more...";
    }   
    }