		<?php
			
		do_action( 'ava_before_footer' );	
			
		global $avia_config;
		$blank = isset($avia_config['template']) ? $avia_config['template'] : "";

		//reset wordpress query in case we modified it
		wp_reset_query();


		//get footer display settings
		$the_id 				= avia_get_the_id(); //use avia get the id instead of default get id. prevents notice on 404 pages
		$footer 				= get_post_meta($the_id, 'footer', true);
		$footer_widget_setting 	= !empty($footer) ? $footer : avia_get_option('display_widgets_socket');


		//check if we should display a footer
		if(!$blank && $footer_widget_setting != 'nofooterarea' )
		{
			if( $footer_widget_setting != 'nofooterwidgets' )
			{
				//get columns
				$columns = avia_get_option('footer_columns');
				$f_bg_image = get_post_meta('3206', 'ms_f_bg_image', true);
				$f_logo = get_post_meta('3206', 'ms_f_logo', true);
				$f_address = get_post_meta('3206', 'ms_f_address', true);
				$f_contact = get_post_meta('3206', 'ms_f_contact', true);
				$f_email = get_post_meta('3206', 'ms_f_email', true);
				$facebook = get_post_meta('3206', 'ms_facebook', true);
				$twitter = get_post_meta('3206', 'ms_twitter', true);
				$google = get_post_meta('3206', 'ms_google', true);
				$skype = get_post_meta('3206', 'ms_skype', true);
				$linkedin = get_post_meta('3206', 'ms_linkedin', true);
				$behance = get_post_meta('3206', 'ms_behance', true);
				$google_photo = get_post_meta('3206', 'ms_google_photo', true);
				$dribbble = get_post_meta('3206', 'ms_dribbble', true);
				$flickr = get_post_meta('3206', 'ms_flickr', true);
				$blogger_b = get_post_meta('3206', 'ms_blogger_b', true);
				$delicious = get_post_meta('3206', 'ms_delicious', true);
		?>
			<footer>
				 <div class="footer-map" style="background-image:none;">
                <div class="footer-bottom">
                    <div class="container-fluid">
                        <div class="row pt-5 d-flex align-items-center justify-content-center">
                            <div class="col-md-5 wow fadeInUp" data-wow-duration="2s">
                                <img src="<?php echo $f_logo;  ?>" alt="#" class="img-fluid">
                            </div>
                            <img class="px-lg-5 mx-lg-5 d-none d-md-block d-lg-block" src="<?php bloginfo('template_directory')?>/assets/dist/images/divider.png" alt="#">
                            <div class="col-md-5 mobile-view">
                                <ul>
                                    <li class="d-flex align-items-center wow fadeInUp" data-wow-duration="2s">
                                        <i class="fas fa-home"></i>
                                        <p class="address ml-3 w-50"><?php echo $f_address; ?></p>
                                    </li>
                                    <li class="d-flex align-items-center wow fadeInUp" data-wow-duration="2s">
                                        <i class="fas fa-headset"></i>
                                        <p class="contact ml-3">
                                        <?php echo $f_contact; ?>
                                            <a class="email-adress d-block" href="<?php echo $f_email; ?>">mail.example@addinweb.com</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    

	<?php   } //endif nofooterwidgets ?>



			

			<?php

			//copyright
			$copyright = do_shortcode( avia_get_option('copyright', "&copy; ".__('Copyright','avia_framework')."  - <a href='".home_url('/')."'>".get_bloginfo('name')."</a>") );

			// you can filter and remove the backlink with an add_filter function
			// from your themes (or child themes) functions.php file if you dont want to edit this file
			// you can also remove the kriesi.at backlink by adding [nolink] to your custom copyright field in the admin area
			// you can also just keep that link. I really do appreciate it ;)
			$kriesi_at_backlink = kriesi_backlink(get_option(THEMENAMECLEAN."_initial_version"), 'Enfold');


			
			if($copyright && strpos($copyright, '[nolink]') !== false)
			{
				$kriesi_at_backlink = "";
				$copyright = str_replace("[nolink]","",$copyright);
			}

			if( $footer_widget_setting != 'nosocket' )
			{

			?>

				<div class="social py-2">
                        <ul class="text-center wow fadeInUp" data-wow-duration="2s">
                            <li>
                                <a href="<?php echo $facebook; ?>" target="blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $twitter; ?>">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $google; ?>">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $skype; ?>">
                                    <i class="fab fa-skype"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $linkedin; ?>">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $behance; ?>">
                                    <i class="fab fa-behance"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $google_photo; ?>">
                                    <i class="fas fa-camera"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $dribbble; ?>">
                                    <i class="fab fa-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $flickr; ?>">
                                    <i class="fab fa-flickr"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $blogger_b; ?>">
                                    <i class="fab fa-blogger-b"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $delicious; ?>">
                                    <i class="fab fa-delicious"></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        	</footer>

			<?php
			} //end nosocket check


		
		
		} //end blank & nofooterarea check
		?>
		<!-- end main -->
		</div>
		
		<?php
		//display link to previeous and next portfolio entry
		echo avia_post_nav();

		echo "<!-- end wrap_all --></div>";


		if(isset($avia_config['fullscreen_image']))
		{ ?>
			<!--[if lte IE 8]>
			<style type="text/css">
			.bg_container {
			-ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale')";
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale');
			}
			</style>
			<![endif]-->
		<?php
			echo "<div class='bg_container' style='background-image:url(".$avia_config['fullscreen_image'].");'></div>";
		}
	?>


<?php




	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */


	wp_footer();


?>
</body>
</html>
