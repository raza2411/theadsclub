
        <!-- footer -->

        <footer>
            <?php
            $f_bg_image = get_post_meta('3206', 'ms_f_bg_image', true);
            $f_logo = get_post_meta('3206', 'ms_f_logo', true);
            $f_address = get_post_meta('3206', 'ms_f_address', true);
            $f_contact = get_post_meta('3206', 'ms_f_contact', true);
            $f_email = get_post_meta('3206', 'ms_f_email', true);
            $facebook = get_post_meta('3206', 'ms_facebook', true);
            $twitter = get_post_meta('3206', 'ms_twitter', true);
            $google = get_post_meta('3206', 'ms_google', true);
            $skype = get_post_meta('3206', 'ms_skype', true);
            $linkedin = get_post_meta('3206', 'ms_linkedin', true);
            $behance = get_post_meta('3206', 'ms_behance', true);
            $google_photo = get_post_meta('3206', 'ms_google_photo', true);
            $dribbble = get_post_meta('3206', 'ms_dribbble', true);
            $flickr = get_post_meta('3206', 'ms_flickr', true);
            $blogger_b = get_post_meta('3206', 'ms_blogger_b', true);
            $delicious = get_post_meta('3206', 'ms_delicious', true);
            ?>
            <div class="footer-map" style="background-image:url('<?php echo $f_bg_image; ?>');">
                <div class="footer-bottom">
                    <div class="container-fluid">
                        <div class="row pt-5 d-flex align-items-center justify-content-center">
                            <div class="col-md-5 wow fadeInUp" data-wow-duration="2s">
                                <img src="<?php echo $f_logo;  ?>" alt="#" class="img-fluid">
                            </div>
                            <img class="px-lg-5 mx-lg-5 d-none d-md-block d-lg-block" src="<?php bloginfo('template_directory')?>/assets/dist/images/divider.png" alt="#">
                            <div class="col-md-5 mobile-view">
                                <ul>
                                    <li class="d-flex align-items-center wow fadeInUp" data-wow-duration="2s">
                                        <i class="fas fa-home"></i>
                                        <p class="address ml-3 w-50"><?php echo $f_address; ?></p>
                                    </li>
                                    <li class="d-flex align-items-center wow fadeInUp" data-wow-duration="2s">
                                        <i class="fas fa-headset"></i>
                                        <p class="contact ml-3">
                                        <?php echo $f_contact; ?>
                                            <a class="email-adress d-block" type="email" href="mailto:<?php echo $f_email; ?>"><?php echo $f_email; ?></a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="social py-2">
                        <ul class="text-center wow fadeInUp" data-wow-duration="2s">
                            <li>
                                <a href="<?php echo $facebook; ?>" target="blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $twitter; ?>">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $google; ?>">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $skype; ?>">
                                    <i class="fab fa-skype"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $linkedin; ?>">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $behance; ?>">
                                    <i class="fab fa-behance"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $google_photo; ?>">
                                    <i class="fas fa-camera"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $dribbble; ?>">
                                    <i class="fab fa-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $flickr; ?>">
                                    <i class="fab fa-flickr"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $blogger_b; ?>">
                                    <i class="fab fa-blogger-b"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $delicious; ?>">
                                    <i class="fab fa-delicious"></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </footer>
        </div>
        <script src="<?php bloginfo('template_directory'); ?>/assets/dist/js/jquery-3.2.1.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/assets/dist/js/jquery.flip.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/assets/dist/js/merge.js"></script>
        <script>
            new WOW().init();

            window.setInterval(function() { //setInterval (loop a function)
                $("#f1_card").toggleClass("flipped"); //toggle class "flipped"
                }, 3000); // Loop it every 5000 milliseconds
        </script>
<?php 
get_footer(home);?>
</body>

</html>