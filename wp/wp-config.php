<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'theadsclub');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[YM>kt*t,N!oo_gK-4]RK[Dhop3tf5t*hI&nmfZ_eBpVq4<M%>n?6{D?UAmalM&m');
define('SECURE_AUTH_KEY',  '{=<V#.Eg}R2E>pq`}@c1YBb[xac!?J%^3pPH&LU6,kCicI7jX`U:c>(0eS2!epWJ');
define('LOGGED_IN_KEY',    'yL[_!rMy!Y1V0KmI%_U>CnSy+i+wO=ceQ{6WEgzi59qV:.+%(5G|X|Q2^N6}-C(5');
define('NONCE_KEY',        '^kU0EcKfWFk73?:sw4PJauh7FakA (}BoX1.lV} tTDh*XK2IW[`cWET{gc17_Q;');
define('AUTH_SALT',        'T76BS8<6J|+D*>>r-bySi#U]ZE4t3mCWe6!+c>b[FJ2A>6}Ns<z7GuJ-4lr^wip|');
define('SECURE_AUTH_SALT', 'jP}=,:@zgAGj<VI9D$gqj5RduU6B3jUPi%nUhjWmd8=$uKLqLR;J^r{=g;=xa(Eu');
define('LOGGED_IN_SALT',   'N^Z^m;s$@q33b~cP(jN&D^b.#N?$|NmgwkM6vPI+<91p)==,IyZS@&sRywoh*m %');
define('NONCE_SALT',       'QSs:sa$BED+YPwRp$nk::2O|dexnQ*>Afo@(@jXx#LQ2,g^I/IzBAOP_jt+P pU9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
