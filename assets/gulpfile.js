// Gulp

var gulp  = require('gulp');

// Plugin

var sass = require('gulp-sass');
var min_css = require('gulp-mini-css');
var concat_js = require('gulp-concat');
var uglyfy = require('gulp-uglify')
var browserSync = require('browser-sync').create();

// Task

gulp.task('sass', function() {
    return gulp.src("src/scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});

gulp.task('concatination', function () {
    return gulp.src('src/js/*.js')
            .pipe(concat_js('merge.js'))
            .pipe(uglyfy())
            .pipe(gulp.dest('dist/js'))
            .pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        proxy: "http://localhost/theadsclub"
    });

    gulp.watch("src/scss/*.scss", ['sass']);
    gulp.watch('src/js/*.js', ['concatination']);
    gulp.watch("../*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);